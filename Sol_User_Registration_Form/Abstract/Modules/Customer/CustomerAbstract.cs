﻿using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.Modules.Customer
{
    public abstract class CustomerAbstract:IDisposable
    {
        public abstract Task<dynamic> CustomerRegistration(ICustomerEntity customerEntityObj);

        public abstract void Dispose();
    }
}
