﻿using Entity.IModel.Person.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.Modules.Employee
{
    public abstract class EmployeeAbstract : IDisposable
    {
        public abstract Task<dynamic> EmployeeRegistration(IEmployeeEntity employeeEntityObj);

        public abstract void Dispose();
    }
}
