﻿using Entity.IModel.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.Modules.Product
{
    public abstract class ProductAbstract:IDisposable
    {
        public abstract void Dispose();

        public abstract Task<dynamic> AddProduct(IProductEntity productEntityObj);
    }
}
