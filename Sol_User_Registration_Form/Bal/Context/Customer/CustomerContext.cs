﻿using Dal.Repository_Facade.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Customer;

namespace Bal.Context.Customer
{
    public class CustomerContext:CustomerRepositoryFacade
    {      

        #region  constructor

        public CustomerContext():base()
        {

        }


        #endregion

        #region public methods

        public async override Task<dynamic> CustomerRegistration(ICustomerEntity customerEntityObj)
        {
            try
            {
                return await base.CustomerRegistration(customerEntityObj);
            }
            catch (Exception)
            {

                throw;
            }

        }       

        #endregion

    }
}
