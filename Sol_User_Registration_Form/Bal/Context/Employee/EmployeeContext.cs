﻿using Dal.Repository_Facade.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Person.Employee;

namespace Bal.Context.Employee
{
    public class EmployeeContext:EmployeeRepositoryFacade
    {
        
        #region  constructor

        public EmployeeContext():base()
        {

        }

        #endregion

        #region public methods

        public async override Task<dynamic> EmployeeRegistration(IEmployeeEntity employeeEntityObj)
        {
            try
            {
                return await base.EmployeeRegistration(employeeEntityObj);
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        #endregion
    }
}
