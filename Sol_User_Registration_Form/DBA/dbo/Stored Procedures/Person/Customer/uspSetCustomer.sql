﻿
CREATE PROCEDURE [dbo].[uspSetCustomer]
	@Command varchar(50)=NOTNULL,

	@PersonId numeric(18,0),
	@FirstName varchar(50),
	@LastName varchar(50),

	@Username varchar(50),
	@Password varchar(50),
	@Usetype varchar(50),

	@MobileNo varchar(10),
	@EmailId varchar(100),	

	@Status int out,
	@Message varchar(max) out
AS

	BEGIN

	DECLARE @ErrorMessage varchar(max)
	DECLARE @Id numeric(18,0)

		IF @Command='INSERT'
		BEGIN

			BEGIN TRANSACTION

				BEGIN TRY

					EXEC uspSetPerson @Command,null,@FirstName,@LastName,@Id out

					EXEC uspSetLogin @Command,@Id,@Username,@Password,@Usetype

					EXEC uspSetCommunication @Command,@Id,@MobileNo,@EmailId

				
					INSERT INTO tblCustomer
					(
					PersonId					
					)
					VALUES
					(
					@Id					
					)

					SET @Status=1
					SET @Message='INSERT SUCCESSFULL'

					COMMIT TRANSACTION				


				END TRY

				BEGIN CATCH

					SET @ErrorMessage=ERROR_MESSAGE()

					SET @Status=0
					SET @Message='INSERT NOT SUCCESSFULL'

					ROLLBACK TRANSACTION

					RAISERROR(@ErrorMessage,16,1)


				END CATCH


		END


	END
	

