﻿CREATE PROCEDURE [dbo].[uspSetCommunication]
	@Command varchar(50)=NOTNULL,

	@PersonId numeric(18,0),
	@MobileNo varchar(10),
	@EmailId varchar(100)
AS

	BEGIN

	DECLARE @ErrorMessage varchar(max)

		IF @Command='INSERT'
		BEGIN

			BEGIN TRANSACTION

				BEGIN TRY
				
					INSERT INTO tblCommunication
					(
					PersonId,
					MobileNo,
					EmailId					
					)
					VALUES
					(
					@PersonId,
					@MobileNo,
					@EmailId					
					)

					COMMIT TRANSACTION				


				END TRY

				BEGIN CATCH

					SET @ErrorMessage=ERROR_MESSAGE()
					ROLLBACK TRANSACTION
					RAISERROR(@ErrorMessage,16,1)


				END CATCH


		END


	END
	

