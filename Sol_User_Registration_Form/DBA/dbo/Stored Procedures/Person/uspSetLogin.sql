﻿CREATE PROCEDURE [dbo].[uspSetLogin]
	@Command varchar(50)=NOTNULL,

	@PersonId numeric(18,0),
	@Username varchar(50),
	@Password varchar(50),
	@Usetype varchar(50)
AS

	BEGIN

	DECLARE @ErrorMessage varchar(max)

		IF @Command='INSERT'
		BEGIN

			BEGIN TRANSACTION

				BEGIN TRY
				
					INSERT INTO tblLogin
					(
					PersonId,
					Username,
					Password,
					Usetype
					)
					VALUES
					(
					@PersonId,
					@Username,
					@Password,
					@Usetype
					)					

					COMMIT TRANSACTION				


				END TRY

				BEGIN CATCH

					SET @ErrorMessage=ERROR_MESSAGE()
					ROLLBACK TRANSACTION
					RAISERROR(@ErrorMessage,16,1)


				END CATCH


		END


	END
	

