﻿CREATE PROCEDURE [dbo].[uspSetPerson]
	@Command varchar(50)=NOTNULL,

	@PersonId numeric(18,0),
	@FirstName varchar(50),
	@LastName varchar(50),

	@Id numeric(18,0) out
AS

	BEGIN

	DECLARE @ErrorMessage varchar(max)

		IF @Command='INSERT'
		BEGIN

			BEGIN TRANSACTION

				BEGIN TRY
				
					INSERT INTO tblPerson
					(					
					FirstName,
					LastName					
					)
					VALUES
					(
					@FirstName,
					@LastName					
					)

					SET @Id=@@IDENTITY
					

					COMMIT TRANSACTION				


				END TRY

				BEGIN CATCH

					SET @ErrorMessage=ERROR_MESSAGE()
					ROLLBACK TRANSACTION
					RAISERROR(@ErrorMessage,16,1)

				END CATCH


		END


	END
	

