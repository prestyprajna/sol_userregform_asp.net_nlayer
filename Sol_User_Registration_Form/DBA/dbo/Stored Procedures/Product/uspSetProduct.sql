﻿CREATE PROCEDURE [dbo].[uspSetProduct]
	@Command Varchar(50),

	@ProductId Numeric(18,0),
	@ProductName Varchar(50),
	@ProductPrice money,
	@QtyAvailable int,

	@Status int out,
	@Message varchar(50) OUT
AS

	BEGIN

	DECLARE @ErrorMessage varchar(max) 

	IF @Command='INSERT'
	BEGIN

		BEGIN TRANSACTION

			BEGIN TRY

				INSERT INTO tblProduct
				(
					ProductName,
					ProductPrice,
					QtyAvailable
				)
				VALUES
				(
					@ProductName,
					@ProductPrice,
					@QtyAvailable
				)

				SET @Status=1
				SET @Message='INSERT SUCCESSFULL'

				COMMIT TRANSACTION


			END TRY


			BEGIN CATCH
				
				SET @ErrorMessage=ERROR_MESSAGE()

				SET @Status=0
				SET @Message='INSERT NOT SUCCESSFULL'

				ROLLBACK TRANSACTION
				
				RAISERROR(@ErrorMessage,16,1)


			END CATCH

	END

	END
	
