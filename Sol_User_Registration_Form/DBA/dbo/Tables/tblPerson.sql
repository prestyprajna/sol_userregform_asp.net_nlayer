﻿CREATE TABLE [dbo].[tblPerson] (
    [PersonId]  NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

