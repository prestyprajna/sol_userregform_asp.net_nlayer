﻿CREATE TABLE [dbo].[tblProduct] (
    [ProductId]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [ProductName]  VARCHAR (50) NULL,
    [ProductPrice] MONEY        NULL,
    [QtyAvailable] INT          NULL,
    PRIMARY KEY CLUSTERED ([ProductId] ASC)
);

