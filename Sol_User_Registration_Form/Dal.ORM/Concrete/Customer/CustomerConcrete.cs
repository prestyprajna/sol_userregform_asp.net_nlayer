﻿using Dal.ORM.Interface.IConcrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Customer;
using Dal.ORM.ORD.Person.Customer;

namespace Dal.ORM.Concrete.Customer
{
    public class CustomerConcrete : ICustomerConcrete
    {
        #region  declaration

        private static Lazy<ICustomerConcrete> _customerConcreteObj = null;

        private Lazy<CustomerDCDataContext> _dc = null;

        #endregion

        #region  constructor

        private CustomerConcrete()
        {

        }

        #endregion

        #region property

        private Lazy<CustomerDCDataContext> CreateDbInstance
        {
            get
            {
                return
                    _dc ??
                    (_dc = new Lazy<CustomerDCDataContext>(() => new CustomerDCDataContext()));
            }
        }

        public static Lazy<ICustomerConcrete> CreateCustomerConcreteInstance
        {
            get
            {
                return
                    _customerConcreteObj ??
                    (_customerConcreteObj = new Lazy<ICustomerConcrete>(() => new CustomerConcrete()));
            }
        }


        #endregion

        #region public methods

        public async Task<dynamic> SetData(string command, ICustomerEntity entityObj, Action<int?, string> storedProcOutPara = null)
        {
            try
            {
                int? status = null;
                string message = null;

                return await Task.Run(() =>
                {
                    var setQuery =
                    CreateDbInstance
                    ?.Value
                    ?.uspSetCustomer(
                        command,
                        entityObj?.personEntityObj?.PersonId,
                        entityObj?.personEntityObj?.FirstName,
                        entityObj?.personEntityObj?.LastName,
                        entityObj?.personEntityObj?.loginEntityObj?.Username,
                        entityObj?.personEntityObj?.loginEntityObj?.Password,
                        entityObj?.personEntityObj?.loginEntityObj?.Usetype,
                        entityObj?.personEntityObj?.communicationEntityObj?.MobileNo,
                        entityObj?.personEntityObj?.communicationEntityObj?.EmailId,
                        ref status,
                        ref message
                        );

                     storedProcOutPara(status, message);

                    return setQuery;
                });
            }
            catch (Exception)
            {

                throw;
            }
           
                
         }

        public void Dispose()
        {

            _dc?.Value?.Dispose();

            _dc = null;
            _customerConcreteObj = null; 
        }

        #endregion
    }
}
