﻿using Dal.ORM.Interface.IConcrete.Employee;
using Dal.ORM.ORD.Person.Employee;
using Entity.IModel.Customer;
using Entity.IModel.Person.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.ORM.Concrete.Employee
{
    public class EmployeeConcrete:IEmployeeConcrete
    {
        #region  declaration

        private static Lazy<EmployeeConcrete> _employeeConcreteObj = null;

        private Lazy<EmployeeDCDataContext> _dc = null;

        #endregion

        #region  constructor

        private EmployeeConcrete()
        {

        }

        #endregion

        #region property

        private Lazy<EmployeeDCDataContext> CreateDbInstance
        {
            get
            {
                return
                    _dc ??
                    (_dc = new Lazy<EmployeeDCDataContext>(() => new EmployeeDCDataContext()));
            }
        }

        public static Lazy<EmployeeConcrete> CreateEmployeeConcreteInstance
        {
            get
            {
                return
                    _employeeConcreteObj ??
                    (_employeeConcreteObj = new Lazy<EmployeeConcrete>(() => new EmployeeConcrete()));
            }
        }


        #endregion

        #region public methods

        public async Task<dynamic> SetData(string command, IEmployeeEntity entityObj, Action<int?, string> storedProcOutPara = null)
        {
            try
            {
                int? status = null;
                string message = null;

                return await Task.Run(() =>
                {
                    var setQuery =
                    CreateDbInstance
                    ?.Value
                    ?.uspSetEmployee(
                        command,
                        entityObj?.personEntityObj?.PersonId,
                        entityObj?.personEntityObj?.FirstName,
                        entityObj?.personEntityObj?.LastName,
                        entityObj?.personEntityObj?.loginEntityObj?.Username,
                        entityObj?.personEntityObj?.loginEntityObj?.Password,
                        entityObj?.personEntityObj?.loginEntityObj?.Usetype,
                        entityObj?.personEntityObj?.communicationEntityObj?.MobileNo,
                        entityObj?.personEntityObj?.communicationEntityObj?.EmailId,
                        ref status,
                        ref message
                        );

                    storedProcOutPara(status, message);

                    return setQuery;
                });
            }
            catch (Exception)
            {

                throw;
            }


        }

        public void Dispose()
        {

            _dc?.Value?.Dispose();

            _dc = null;
            _employeeConcreteObj = null;
        }       

        #endregion
    }
}

