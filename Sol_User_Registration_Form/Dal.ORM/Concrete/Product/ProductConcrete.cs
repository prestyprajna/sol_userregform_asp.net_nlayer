﻿using Dal.ORM.Interface.IConcrete.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Product;
using Dal.ORM.ORD.Product;

namespace Dal.ORM.Concrete.Product
{
    public class ProductConcrete : IProductConcrete
    {
        #region  declaration

        private static Lazy<IProductConcrete> _productConcreteObj = null;
        private Lazy<ProductDCDataContext> _dc = null;

        #endregion

        #region  constructor

        private ProductConcrete()
        {

        }

        #endregion

        #region  property

        private Lazy<ProductDCDataContext> CreateDCInstance
        {
            get
            {
                return
                    _dc 
                    ??
                    (_dc = new Lazy<ProductDCDataContext>(() => new ProductDCDataContext()));
            }
        }

        public static Lazy<IProductConcrete> CreateProductInstance
        {

            get
            {
                return
                    _productConcreteObj
                    ??
                    (_productConcreteObj=new Lazy<IProductConcrete>(()=>new ProductConcrete()));
            }
        }

        #endregion

        #region  public methods

        public async Task<dynamic> SetData(string command, IProductEntity entityObj, Action<int?, string> storedProcOutPara = null)
        {
            try
            {
                int? status = null;
                string message = null;

                return await Task.Run(() =>
                {
                    var setQuery =
                    CreateDCInstance
                    ?.Value
                    ?.uspSetProduct(
                        command,
                        entityObj?.ProductId,
                        entityObj?.ProductName,
                        entityObj?.ProductPrice,
                        entityObj?.QtyAvailable,
                        ref status,
                        ref message
                        );

                    storedProcOutPara(status, message);

                    return setQuery;

                });
            }
            catch (Exception)
            {

                throw;
            }         

           
        }

        #endregion

    }
}
