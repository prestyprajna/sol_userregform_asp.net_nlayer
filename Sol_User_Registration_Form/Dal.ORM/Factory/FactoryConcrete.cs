﻿using Dal.ORM.Concrete.Customer;
using Dal.ORM.Concrete.Employee;
using Dal.ORM.Concrete.Product;
using Dal.ORM.Interface.IConcrete;
using Dal.ORM.Interface.IConcrete.Employee;
using Dal.ORM.Interface.IConcrete.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.ORM.Factory
{
    public static class FactoryConcrete
    {
        #region  enum

        public enum ConcreteType
        {
            Customer=0,
            Employee=1,
            Product=2
        };

        #endregion

        #region  declaration

        private static Dictionary<ConcreteType, dynamic> _dicObj = new Dictionary<ConcreteType, dynamic>();

        #endregion

        #region constructor

        static FactoryConcrete()
        {
            AddInstance();
        }

        #endregion

        #region private method
        
        private static void AddInstance()
        {
            _dicObj.Add(ConcreteType.Customer, new Lazy<ICustomerConcrete>(() => CustomerConcrete.CreateCustomerConcreteInstance.Value));
            _dicObj.Add(ConcreteType.Employee, new Lazy<IEmployeeConcrete>(() => EmployeeConcrete.CreateEmployeeConcreteInstance.Value));
            _dicObj.Add(ConcreteType.Product, new Lazy<IProductConcrete>(() => ProductConcrete.CreateProductInstance.Value));
        }

        #endregion

        #region public method

        public static TConcrete ExecuteConcrete<TConcrete>(ConcreteType concreteTypeObj) where TConcrete:class
        {
            return
                _dicObj[concreteTypeObj].Value as TConcrete;
        }

        #endregion
    }
}
