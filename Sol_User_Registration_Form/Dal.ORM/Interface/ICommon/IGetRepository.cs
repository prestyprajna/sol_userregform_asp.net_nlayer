﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.ORM.Interface.ICommon
{
    public interface IGetRepository<TLinqEntity,TEntity> where TLinqEntity : class where TEntity : class
    {
        Task<dynamic> GetData(string command, TEntity entityObj,Func<TLinqEntity, TEntity> funcObj,Action<int?, string> storedProcOutPara = null);
    }
}
