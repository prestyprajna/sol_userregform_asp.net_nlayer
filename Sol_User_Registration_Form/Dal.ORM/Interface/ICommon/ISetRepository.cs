﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.ORM.Interface.ICommon
{
    public interface ISetRepository<TEntity> where TEntity : class
    {
        Task<dynamic> SetData(string command, TEntity entityObj, Action<int?, string> storedProcOutPara = null);
    }
}
