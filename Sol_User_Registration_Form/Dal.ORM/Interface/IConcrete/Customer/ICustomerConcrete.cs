﻿using Dal.ORM.Interface.ICommon;
using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.ORM.Interface.IConcrete
{
    public interface ICustomerConcrete:ISetRepository<ICustomerEntity>,IDisposable
    {
    }
}
