﻿using Dal.ORM.Interface.ICommon;
using Entity.IModel.Customer;
using Entity.IModel.Person.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.ORM.Interface.IConcrete.Employee
{
    public interface IEmployeeConcrete : ISetRepository<IEmployeeEntity>,IDisposable
    {
    }
}
