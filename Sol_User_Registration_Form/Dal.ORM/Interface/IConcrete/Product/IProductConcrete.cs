﻿using Dal.ORM.Interface.ICommon;
using Entity.IModel.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.ORM.Interface.IConcrete.Product
{
    public interface IProductConcrete:ISetRepository<IProductEntity>
    {
    }
}
