﻿using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Repository.IRepository.Customer
{
    public interface IAddCustomerRepository
    {
        Task<dynamic> AddCustomerRegistration(ICustomerEntity customerEntityObj);
    }
}
