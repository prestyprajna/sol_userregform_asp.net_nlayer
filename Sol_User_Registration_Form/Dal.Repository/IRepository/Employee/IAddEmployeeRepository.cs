﻿using Entity.IModel.Customer;
using Entity.IModel.Person.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Repository.IRepository.Employee
{
    public interface IAddEmployeeRepository
    {
        Task<dynamic> AddEmployeeRegistration(IEmployeeEntity employeeEntityObj);
    }
}
