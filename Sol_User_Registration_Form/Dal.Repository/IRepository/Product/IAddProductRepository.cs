﻿using Entity.IModel.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Repository.IRepository.Product
{
    public interface IAddProductRepository
    {
        Task<dynamic> ProductRegistration(IProductEntity productEntityObj);
    }
}
