﻿using Dal.Repository.Repository.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Repository.Repository_Factory
{
    public static class CustomerRepositoryFactory
    {
        #region  enum

        public enum CustomerRepositoryType
        {
            AddCustomerRepository=0
        };

        #endregion

        #region  declaration

        private static Dictionary<CustomerRepositoryType, dynamic> _dicObj= new Dictionary<CustomerRepositoryType,dynamic>();

        #endregion

        #region  constructor

        static CustomerRepositoryFactory()
        {
            AddInstance();
        }

        #endregion

        #region  private method

        private static void AddInstance()
        {
            _dicObj.Add(CustomerRepositoryType.AddCustomerRepository, new Lazy<AddCustomerRepository>(() => AddCustomerRepository.CreateAddCustomerRepositoryInstance.Value));
        }

        #endregion

        #region  public method

        public static TCustomerRepository ExecuteRepository<TCustomerRepository>(CustomerRepositoryType customerRepositoryTypeObj) where TCustomerRepository:class
        {
            return
                _dicObj[customerRepositoryTypeObj].Value as TCustomerRepository;
        }
        #endregion
    }
}
