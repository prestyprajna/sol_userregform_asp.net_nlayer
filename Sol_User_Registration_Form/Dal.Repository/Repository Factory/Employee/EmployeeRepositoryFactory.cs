﻿using Dal.Repository.Repository.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Repository.Repository_Factory
{
    public static class EmployeeRepositoryFactory
    {
        #region  enum

        public enum EmployeeRepositoryType
        {
            AddEmployeeRepository = 0
        };

        #endregion

        #region  declaration

        private static Dictionary<EmployeeRepositoryType, dynamic> _dicObj = new Dictionary<EmployeeRepositoryType, dynamic>();

        #endregion

        #region  constructor
        static EmployeeRepositoryFactory()
        {
            AddInstance();
        }

        #endregion

        #region  private method

        private static void AddInstance()
        {
            _dicObj.Add(EmployeeRepositoryType.AddEmployeeRepository, new Lazy<AddEmployeeRepository>(() =>AddEmployeeRepository.CreateAddEmployeeRepositoryInstance.Value));
        }

        #endregion

        #region  public method

        public static TEmployeeRepository ExecuteRepository<TEmployeeRepository>(EmployeeRepositoryType employeeRepositoryTypeObj) where TEmployeeRepository:class
        {
            return
                _dicObj[employeeRepositoryTypeObj].Value as TEmployeeRepository;
        }

        #endregion
    }
}
