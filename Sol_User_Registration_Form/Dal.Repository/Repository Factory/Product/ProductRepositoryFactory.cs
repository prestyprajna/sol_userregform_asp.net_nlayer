﻿using Dal.Repository.Repository.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Repository.Repository_Factory.Product
{
    public static class ProductRepositoryFactory
    {
        #region  enum

        public enum RepositoryType
        {
            AddProductRepository=0
        };

        #endregion

        #region  declaration

        private static Dictionary<RepositoryType, dynamic> _dicObj = new Dictionary<RepositoryType, dynamic>();

        #endregion

        #region  constructor
        static ProductRepositoryFactory()
        {
            AddInstance();
        }

        #endregion

        #region  private method

        private static void AddInstance()
        {
            _dicObj.Add(RepositoryType.AddProductRepository,new Lazy<AddProductRepository>(()=>AddProductRepository.CreateAddRepositoryInstance.Value));
        }

        #endregion

        #region  public method

        public static TRepository ExecuteRepository<TRepository>(RepositoryType repositoryTypeObj) where TRepository:class
        {
            return
                _dicObj[repositoryTypeObj].Value as TRepository;
        }

        #endregion


    }
}
