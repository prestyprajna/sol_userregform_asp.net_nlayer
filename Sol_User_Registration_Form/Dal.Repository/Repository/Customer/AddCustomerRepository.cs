﻿using Dal.Repository.IRepository.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Customer;
using Dal.ORM.Concrete.Customer;
using Dal.ORM.Factory;
using static Dal.ORM.Factory.FactoryConcrete;
using Dal.ORM.Interface.IConcrete;

namespace Dal.Repository.Repository.Customer
{
    public class AddCustomerRepository : IAddCustomerRepository
    {
        #region  declaration

        private static Lazy<AddCustomerRepository> _addCustomerRepositoryObj = null;

        private Lazy<ICustomerConcrete> _customerConcreteObj = null;

        #endregion

        #region  constructor

        private AddCustomerRepository()
        {

        }

        #endregion

        #region property

        private Lazy<ICustomerConcrete> CreateCustmerConcreteInstance
        {
            get
            {
                return
                    _customerConcreteObj ??
                    (_customerConcreteObj = new Lazy<ICustomerConcrete>(() =>FactoryConcrete.ExecuteConcrete<ICustomerConcrete>(FactoryConcrete.ConcreteType.Customer)));
            }
        }

        public static Lazy<AddCustomerRepository> CreateAddCustomerRepositoryInstance
        {
            get
            {
                return
                    _addCustomerRepositoryObj ??
                    (_addCustomerRepositoryObj = new Lazy<AddCustomerRepository>(() => new AddCustomerRepository()));
            }
        }

        #endregion

        #region  public methods

        public async Task<dynamic> AddCustomerRegistration(ICustomerEntity customerEntityObj)
        {
            int? status = null;
            string message = null;

            try
            {
                var setQuery = await CreateCustmerConcreteInstance
               ?.Value
               ?.SetData(
                   "INSERT",
                   customerEntityObj,
                   (leStatus, leMessage) =>
                   {
                       status = leStatus;
                       message = leMessage;
                   });

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {

                throw;
            }              
                
        }

        #endregion
    }
}
