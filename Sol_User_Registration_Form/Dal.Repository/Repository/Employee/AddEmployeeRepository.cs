﻿using Dal.Repository.IRepository.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Customer;
using Dal.ORM.Interface.IConcrete;
using Dal.ORM.Factory;
using Dal.ORM.Interface.IConcrete.Employee;
using Entity.IModel.Person.Employee;

namespace Dal.Repository.Repository.Employee
{
    public class AddEmployeeRepository : IAddEmployeeRepository
    {
        #region  declaration

        private static Lazy<AddEmployeeRepository> _addEmployeeRepositoryObj = null;

        private Lazy<IEmployeeConcrete> _employeeConcreteObj = null;

        #endregion

        #region  constructor

        private AddEmployeeRepository()
        {

        }

        #endregion

        #region property

        private Lazy<IEmployeeConcrete> CreateEmployeeConcreteInstance
        {
            get
            {
                return
                    _employeeConcreteObj ??
                    (_employeeConcreteObj = new Lazy<IEmployeeConcrete>(() => FactoryConcrete.ExecuteConcrete<IEmployeeConcrete>(FactoryConcrete.ConcreteType.Employee)));
            }
        }

        public static Lazy<AddEmployeeRepository> CreateAddEmployeeRepositoryInstance
        {
            get
            {
                return
                    _addEmployeeRepositoryObj ??
                    (_addEmployeeRepositoryObj = new Lazy<AddEmployeeRepository>(() => new AddEmployeeRepository()));
            }
        }

        #endregion

        #region  public methods     

        public async Task<dynamic> AddEmployeeRegistration(IEmployeeEntity employeeEntityObj)
        {
            int? status = null;
            string message = null;

            try
            {
                var setQuery = await CreateEmployeeConcreteInstance
               ?.Value
               ?.SetData(
                   "INSERT",
                   employeeEntityObj,
                   (leStatus, leMessage) =>
                   {
                       status = leStatus;
                       message = leMessage;
                   });

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
