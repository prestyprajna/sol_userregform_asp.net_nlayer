﻿using Dal.Repository.IRepository.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Product;
using Dal.ORM.Concrete.Product;
using Dal.ORM.Factory;
using Dal.ORM.Interface.IConcrete.Product;

namespace Dal.Repository.Repository.Product
{
    public class AddProductRepository : IAddProductRepository
    {
        #region  declaration

        private static Lazy<AddProductRepository> _addRepositoryObj = null;       
        private Lazy<ProductConcrete> _productConcreteObj = null;

        #endregion

        #region  constructor

        private AddProductRepository()
        {

        }

        #endregion

        #region  property

        public static Lazy<AddProductRepository> CreateAddRepositoryInstance
        {
            get
            {
                return
                    _addRepositoryObj
                    ??
                    (_addRepositoryObj = new Lazy<AddProductRepository>(() => new AddProductRepository()));
            }
        }

        private Lazy<ProductConcrete> CreateProductConcreteInstance
        {
            get
            {
                return
                    _productConcreteObj
                    ??
                    (_productConcreteObj = new Lazy<ProductConcrete>(() => FactoryConcrete.ExecuteConcrete<ProductConcrete>(FactoryConcrete.ConcreteType.Product)));

            }
        }

        #endregion

        #region  methods

        public async Task<dynamic> ProductRegistration(IProductEntity productEntityObj)
        {
            try
            {
                int? status = null;
                string message = null;

                var setQuery=await CreateProductConcreteInstance
                     ?.Value
                     ?.SetData(
                         "INSERT",
                         productEntityObj,
                         (leStatus, leMessage) =>
                         {
                             status = leStatus;
                             message = leMessage;
 
                         }
                         );

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        #endregion

    }
}
