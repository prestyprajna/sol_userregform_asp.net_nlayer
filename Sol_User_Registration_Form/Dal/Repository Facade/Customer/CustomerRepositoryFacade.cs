﻿using Abstract.Modules.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Customer;
using Dal.Repository.Repository.Customer;
using Dal.Repository.Repository_Factory;

namespace Dal.Repository_Facade.Customer
{
    public class CustomerRepositoryFacade : CustomerAbstract
    {
        #region declaration

        private Lazy<AddCustomerRepository> _addCustomerRepositoryObj = null;

        #endregion

        #region constructor

        public CustomerRepositoryFacade()
        {

        }      

        #endregion

        #region  property

        private Lazy<AddCustomerRepository> AddCustomerRepositoryCreateInstance
        {
            get
            {
                return
                    _addCustomerRepositoryObj
                    ??
                    (_addCustomerRepositoryObj = new Lazy<AddCustomerRepository>(() =>CustomerRepositoryFactory.ExecuteRepository<AddCustomerRepository>(CustomerRepositoryFactory.CustomerRepositoryType.AddCustomerRepository)));
            }
        
        }


        #endregion

        #region public methods

        public async override Task<dynamic> CustomerRegistration(ICustomerEntity customerEntityObj)
        {
            try
            {
                return await AddCustomerRepositoryCreateInstance
               ?.Value
               ?.AddCustomerRegistration(customerEntityObj);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        public override void Dispose()
        {
            _addCustomerRepositoryObj = null;
        }


        #endregion

    }
}
