﻿using Abstract.Modules.Employee;
using Dal.Repository.Repository.Employee;
using Dal.Repository.Repository_Factory;
using Entity.IModel.Person.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Repository_Facade.Employee
{
    public class EmployeeRepositoryFacade : EmployeeAbstract
    {
        #region declaration

        private Lazy<AddEmployeeRepository> _addEmployeeRepositoryObj = null;

        #endregion

        #region constructor

        public EmployeeRepositoryFacade()
        {

        }

        #endregion

        #region  property

        private Lazy<AddEmployeeRepository> AddEmployeeRepositoryCreateInstance
        {
            get
            {
                return
                    _addEmployeeRepositoryObj
                    ??
                    (_addEmployeeRepositoryObj = new Lazy<AddEmployeeRepository>(() => EmployeeRepositoryFactory.ExecuteRepository<AddEmployeeRepository>(EmployeeRepositoryFactory.EmployeeRepositoryType.AddEmployeeRepository)));


            }
        }


        #endregion

        #region public methods

        public async override Task<dynamic> EmployeeRegistration(IEmployeeEntity employeeEntityObj)
        {
            try
            {
                return await AddEmployeeRepositoryCreateInstance
                    ?.Value
                    ?.AddEmployeeRegistration(employeeEntityObj);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public override void Dispose()
        {
            _addEmployeeRepositoryObj = null;
        }

        #endregion
    }
}
