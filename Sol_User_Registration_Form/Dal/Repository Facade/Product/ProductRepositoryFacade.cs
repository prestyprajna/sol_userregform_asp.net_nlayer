﻿using Abstract.Modules.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Product;
using Dal.Repository.Repository.Product;
using Dal.Repository.Repository_Factory.Product;

namespace Dal.Repository_Facade.Product
{
    public class ProductRepositoryFacade : ProductAbstract
    {
        #region  declaration

        private Lazy<AddProductRepository> _addProductRepositoryObj = null;      
        #endregion

        #region constructor
        public ProductRepositoryFacade()
        {

        }

        #endregion

        #region  property

        private Lazy<AddProductRepository> CreateAddProductRepositoryInstance
        {
            get
            {
                return
                    _addProductRepositoryObj
                    ??
                    (_addProductRepositoryObj = new Lazy<AddProductRepository>(() => ProductRepositoryFactory.ExecuteRepository<AddProductRepository>(ProductRepositoryFactory.RepositoryType.AddProductRepository)));

            }
        }

        #endregion

        #region public methods

        public async override Task<dynamic> AddProduct(IProductEntity productEntityObj)
        {
            try
            {
                return await CreateAddProductRepositoryInstance
               ?.Value
               ?.ProductRegistration(productEntityObj);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        public override void Dispose()
        {
            _addProductRepositoryObj = null;
        }

        #endregion

    }
}
