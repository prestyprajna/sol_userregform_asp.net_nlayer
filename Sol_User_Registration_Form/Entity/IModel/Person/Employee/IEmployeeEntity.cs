﻿using Entity.Model.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.IModel.Person.Employee
{
    public interface IEmployeeEntity
    {
        decimal? EmployeeId { get; set; }

        PersonEntity personEntityObj { get; set; }
    }
}
