﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.IModel.Person
{
    public interface ILoginEntity
    {
        decimal? PersonId { get; set; }

        string Username { get; set; }

        string Password { get; set; }

        string Usetype { get; set; }
    }
}
