﻿using Entity.Model.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.IModel.Person
{
    public interface IPersonEntity
    {
        decimal? PersonId { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

        LoginEntity loginEntityObj { get; set; }

        CommunicationEntity communicationEntityObj { get; set; }       

    }
}
