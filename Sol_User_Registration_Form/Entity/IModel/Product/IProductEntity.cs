﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.IModel.Product
{
    public interface IProductEntity
    {
        decimal? ProductId { get; set; }

        string ProductName { get; set; }

        long? ProductPrice { get; set; }

        int? QtyAvailable { get; set; }
    }
}
