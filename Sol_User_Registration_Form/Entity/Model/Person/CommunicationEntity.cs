﻿using Entity.IModel.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Person
{
    [DataContract]
    public class CommunicationEntity: ICommunicationEntity
    {
        [DataMember(EmitDefaultValue = false)]
        public decimal? PersonId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MobileNo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EmailId { get; set; }
    }
}
