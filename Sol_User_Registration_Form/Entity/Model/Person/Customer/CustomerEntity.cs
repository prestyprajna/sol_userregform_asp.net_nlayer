﻿using Entity.IModel.Customer;
using Entity.IModel.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Person.Customer
{
    [DataContract]
    public class CustomerEntity: ICustomerEntity
    {
        [DataMember(EmitDefaultValue = false)]
        public decimal? CustomerId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public PersonEntity personEntityObj { get; set; }
    }
}
