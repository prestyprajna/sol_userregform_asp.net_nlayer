﻿using Entity.IModel.Person;
using Entity.IModel.Person.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Person.Employee
{
    [DataContract]
    public class EmployeeEntity: IEmployeeEntity
    {
        [DataMember(EmitDefaultValue = false)]
        public decimal? EmployeeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public PersonEntity personEntityObj { get; set; }
    }
}
