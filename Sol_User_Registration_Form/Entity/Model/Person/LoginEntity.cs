﻿using Entity.IModel.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Person
{
    [DataContract]
    public class LoginEntity: ILoginEntity
    {
        [DataMember(EmitDefaultValue = false)]
        public decimal? PersonId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Username { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Password { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Usetype { get; set; }
    }
}
