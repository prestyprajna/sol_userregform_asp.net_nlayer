﻿using Entity.IModel.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Person
{
    [DataContract]
    public class PersonEntity: IPersonEntity
    {
        [DataMember(EmitDefaultValue = false)]
        public decimal? PersonId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FirstName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public LoginEntity loginEntityObj { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public CommunicationEntity communicationEntityObj { get; set; }

    }
}
