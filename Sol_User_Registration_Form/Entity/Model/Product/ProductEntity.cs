﻿using Entity.IModel.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Product
{
    [DataContract]
    public class ProductEntity: IProductEntity
    {
        [DataMember(EmitDefaultValue = false)]
        public decimal? ProductId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProductName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public long? ProductPrice { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? QtyAvailable { get; set; }
    }
}
