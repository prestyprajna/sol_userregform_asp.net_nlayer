﻿using Bal.Context.Customer;
using Entity.Model.Person;
using Entity.Model.Person.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Asp.net
{
    public partial class Customer_Page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void btnSubmit_Click(object sender, EventArgs e)
        {
            bool result = await this.RegisterCustomerData();

            await this.CheckInsertSuccesssfull(result);
        }


        #region   private methods

        private async Task<bool> RegisterCustomerData()  //used to bind data from textbox to entity
        {            
            CustomerEntity customerEntityObj = new CustomerEntity()
            {
                personEntityObj = new PersonEntity()
                {
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,
                    loginEntityObj = new LoginEntity()
                    {
                        Username = txtUsername.Text,
                        Password = txtPassword.Text,
                        Usetype = txtUsetype.Text
                    },
                    communicationEntityObj = new CommunicationEntity()
                    {
                        EmailId = txtEmailId.Text,
                        MobileNo = txtMobileNo.Text
                    }
                }
            };
            var flag = await new CustomerContext().CustomerRegistration(customerEntityObj);

            return flag;
        }

        private async Task CheckInsertSuccesssfull(bool val)
        {
            await Task.Run(() =>
            {
                if (val == true)
                {
                    Response.Redirect("~/LogoutPage.aspx");
                }
                else
                {
                    lblMessage.Text = "*please enter the data again";
                }
            });
           
        }

        #endregion
    }
}