﻿using Bal.Context.Customer;
using Entity.Model.Person;
using Entity.Model.Person.Customer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UTP.Modules.Customer
{
    [TestClass]
    public class CustomerUnitTest
    {  
        [TestMethod]
        public void CustomerTestMethod()  //add customer data
        {
            Task.Run(async () =>
            {
                var customerEntityObj=new CustomerEntity()
                {
                    personEntityObj = new PersonEntity()
                    {
                        FirstName = "presty",
                        LastName = "prajna",
                        loginEntityObj = new LoginEntity()
                        {
                            Username = "presty123",
                            Password = "123",
                            Usetype = "Customer"
                        },
                        communicationEntityObj = new CommunicationEntity()
                        {
                            MobileNo = "7039587195",
                            EmailId = "presty@gmail.com"
                        }
                    }
                };

                var result=await new CustomerContext()?.CustomerRegistration(customerEntityObj);

                Assert.IsTrue(result);

            }).Wait();
        }

    }
}
