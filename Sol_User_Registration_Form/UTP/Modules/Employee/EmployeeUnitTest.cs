﻿using Bal.Context.Employee;
using Entity.Model.Person;
using Entity.Model.Person.Employee;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTP.Modules.Employee
{
    [TestClass]
    public class EmployeeUnitTest
    {
        [TestMethod]
        public void EmployeeTestMethod()  //add employee data
        {
            Task.Run(async () =>
            {
                var employeeEntityObj = new EmployeeEntity()
                {
                    personEntityObj = new PersonEntity()
                    {
                        FirstName = "deepak",
                        LastName = "kotian",
                        loginEntityObj = new LoginEntity()
                        {
                            Username = "deepak123",
                            Password = "123",
                            Usetype = "Employee-Sales"
                        },
                        communicationEntityObj = new CommunicationEntity()
                        {
                            MobileNo = "7039587196",
                            EmailId = "deepak@gmail.com"
                        }
                    }
                };

                var result = await new EmployeeContext()?.EmployeeRegistration(employeeEntityObj);

                Assert.IsTrue(result);

            }).Wait();
        }
    }
}
