﻿using Bal.Context.Product;
using Entity.IModel.Product;
using Entity.Model.Product;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTP.Modules.Product
{
    [TestClass]
    public class ProductUnitTest
    {
        [TestMethod] 
        public void ProductTestMethod()   //add product data
        {
            try
            {
                Task.Run(async () =>
                {                   

                    IProductEntity productEntityObj = new ProductEntity()
                    {
                        ProductName = "TURMERIC",
                        ProductPrice = 50,
                        QtyAvailable = 50
                    };                    

                    var result = await new ProductContext().AddProduct(productEntityObj);

                    Assert.IsTrue(result);

                }).Wait();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
